using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.XR.Interaction.Toolkit;

public class BalloonInflator : XRGrabInteractable
{
    [Header("Balloon Data")] 
    public Transform attachPoint;
    public Balloon balloonPrefab;
    public PlayableDirector playableDirector;
    
    //bool m_Animating = false;
    private Balloon m_BalloonInstance;
    private XRBaseController m_Controller;

    protected override void OnSelectEntered(SelectEnterEventArgs args)
    {
        base.OnSelectEntered(args);

        m_BalloonInstance = Instantiate(balloonPrefab, attachPoint);
        
        var controllerInteractor = args.interactorObject as XRBaseControllerInteractor;
        m_Controller = controllerInteractor.xrController;

        m_Controller.SendHapticImpulse(0.5f, 0.2f);

        //m_Animating = true;
        playableDirector.Play();
    }

    protected override void OnSelectExited(SelectExitEventArgs args)
    {
        base.OnSelectExited(args);

        //m_Animating = false;
        playableDirector.Stop();
        Destroy(m_BalloonInstance.gameObject);
    }

    public override void ProcessInteractable(XRInteractionUpdateOrder.UpdatePhase updatePhase)
    {
        base.ProcessInteractable(updatePhase);

        if (isSelected && m_Controller != null)
        {
            m_BalloonInstance.transform.localScale =
                Vector3.one * Mathf.Lerp(1.0f, 4.0f, m_Controller.activateInteractionState.value);
            m_Controller.SendHapticImpulse(m_Controller.activateInteractionState.value, 0.1f);

            playableDirector.time = m_Controller.activateInteractionState.value;
        }
    }
}
