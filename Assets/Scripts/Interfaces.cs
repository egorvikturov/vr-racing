public static  class Interfaces
{
    public interface IMovable
    {
        void Move(float ratio);
        void StopMoving();
        void ApplyAcceleration();
    }

    public interface IBreakable
    {
        void Brake();
        void StopBraking();
        void ApplyBrakeForce();
    }

    public interface IRotatable
    {
        void Rotate(float ratio);
        void StopRotating();
        void ApplyRotate(float angle);
    }
}
