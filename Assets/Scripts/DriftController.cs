using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Debug = FMOD.Debug;

public class DriftContoller : MonoBehaviour
{
    [Header("Car Settings")]
    [SerializeField] private float acceleration = 50f;
    [SerializeField] private float drag = 0.98f;
    [SerializeField] private float maxSpeed = 15f;
    [SerializeField] private float maxTurnAngle = 45f;
    [SerializeField] private float traction = 1f;
    
    private Vector3 moveForce;
    private float verticalRatio = 0f; 
    private float horizontalRatio = 0f; 
    
    private void FixedUpdate()
    {
        verticalRatio = Input.GetAxis("Vertical");
        horizontalRatio = Input.GetAxis("Horizontal");
        
        moveForce += transform.forward * acceleration * verticalRatio * Time.deltaTime;
        transform.position += moveForce * Time.deltaTime;
        
        transform.Rotate(Vector3.up * horizontalRatio * moveForce.magnitude * maxTurnAngle * Time.deltaTime);
        
        moveForce *= drag;
        moveForce = Vector3.ClampMagnitude(moveForce, maxSpeed);

        // Traction
        moveForce = Vector3.Lerp(moveForce.normalized, transform.forward, traction * Time.deltaTime) * moveForce.magnitude;
    }
}
