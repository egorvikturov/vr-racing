using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelController : MonoBehaviour
{
    private float verticalRatio = 0f; 
    private float horizontalRatio = 0f; 
    
    private void FixedUpdate()
    {
        verticalRatio = Input.GetAxis("Vertical");
        horizontalRatio = Input.GetAxis("Horizontal");
        
        if (gameObject.TryGetComponent(out Car car))
        {
            if (verticalRatio != 0f)
            {
                car.Move(verticalRatio);
            }
            else
            {
                car.StopMoving();
            }

            if (horizontalRatio != 0)
            {
                car.Rotate(horizontalRatio);
            }
            else
            {
                car.StopRotating();
            }
            
            if (Input.GetKey(KeyCode.Space))
            {
                car.Brake();
            }
            else
            {
                car.StopBraking();
            }
        }
        
        

        
    }
}
