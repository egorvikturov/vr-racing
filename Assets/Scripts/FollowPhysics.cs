using UnityEngine;

public class FollowPhysics : MonoBehaviour
{
    [SerializeField] private Transform target;
    
    void FixedUpdate()
    {
        gameObject.transform.position = target.position;
        gameObject.transform.rotation = target.rotation;
    }
}

