using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.Controls;
using UnityEngine.Serialization;
using UnityEngine.XR.Interaction.Toolkit;

public class SteeringWheel : XRGrabInteractable
{
    [Header("Steering wheel settings")]
    [SerializeField] private Transform steeringWheel;
    [SerializeField] private Car car;
    [SerializeField] private float maxAngleTurn = 90;

    private float angleInspector;
    private float horizontalRatio;
    private float verticalRatio = 1;

    public void ReturnPosition()
    {
        transform.position = steeringWheel.position;
        transform.rotation = steeringWheel.rotation;
    }

    private void FixedUpdate()
    {
        angleInspector = Mathf.Repeat(gameObject.transform.eulerAngles.z + 180, 360) - 180; // (-180; 180)
        horizontalRatio = Mathf.Lerp(-1, 1, Mathf.InverseLerp(-maxAngleTurn, maxAngleTurn, Mathf.Clamp(angleInspector, -maxAngleTurn, maxAngleTurn)) ); // (-1; 1)
        //Debug.Log(horizontalRatio);
        car.Rotate(horizontalRatio);
    }

    public void ToggleForwardGear()
    {
        verticalRatio = 1;
    }
    
    public void ToggleReverseGear()
    {
        verticalRatio = -1;
    }

    public void Move()
    {
        car.Move(verticalRatio);
        //Debug.Log(verticalRatio);
    }
}
