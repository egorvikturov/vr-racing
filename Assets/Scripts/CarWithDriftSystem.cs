using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarWithDriftSystem : MonoBehaviour, Interfaces.IMovable, Interfaces.IBreakable, Interfaces.IRotatable
{
    [Header("Wheels Transforms")]
    [SerializeField] private Transform frontRightTransform;
    [SerializeField] private Transform frontLeftTransform;
    [SerializeField] private Transform backRightTransform;
    [SerializeField] private Transform backLeftTransform;

    [Header("Steering Wheel")] 
    [SerializeField] private Transform steeringWheel;

    [Header("Car Settings")]
    [SerializeField] private float acceleration = 50f;
    [SerializeField] private float drag = 0.98f;
    [SerializeField] private float maxSpeed = 15f;
    [SerializeField] private float brakingForce = 300f;
    [SerializeField] private float maxTurnAngle = 45f;

    private Vector3 moveForce;
    private Vector3 currentAcceleration;
    private float currentBrakeForce = 0f;
    private float currentTurnAngle = 0f;

    private void FixedUpdate()
    {
        // UpdateWheel(frontRight, frontRightTransform);
        // UpdateWheel(frontLeft, frontLeftTransform);
        // UpdateWheel(backRight, backRightTransform);
        // UpdateWheel(backLeft, backLeftTransform);
    }

    public void Move(float ratio)
    {
        // Move
        moveForce += transform.forward * acceleration * ratio * Time.deltaTime;
        currentAcceleration = moveForce;
        
        
        // Drag and max speed
        currentAcceleration *= drag;
        currentAcceleration = Vector3.ClampMagnitude(moveForce, maxSpeed);
        ApplyAcceleration();
    }

    public void Brake()
    {
        //Debug.Log("Stop!");
        currentBrakeForce = brakingForce;
        ApplyBrakeForce();
    }

    public void StopBraking()
    {
        currentBrakeForce = 0f;
        ApplyBrakeForce();
    }

    public void StopMoving()
    {
        currentAcceleration = new Vector3(0, 0, 0);
        ApplyAcceleration();
    }

    public void ApplyAcceleration()
    {
        transform.position += currentAcceleration * Time.deltaTime;
    }

    public void ApplyBrakeForce()
    {
        //backRight.brakeTorque = currentBrakeForce;
        //backLeft.brakeTorque = currentBrakeForce;
        //frontRight.brakeTorque = currentBrakeForce;
        //frontLeft.brakeTorque = currentBrakeForce;
        
    }

    public void Rotate(float ratio)
    {
        transform.Rotate(Vector3.up * ratio * moveForce.magnitude * maxTurnAngle * Time.deltaTime);
        ApplyRotate(currentTurnAngle);
        //Debug.Log("I Rotate = " + currentTurnAngle);
    }

    public void StopRotating()
    {
        currentTurnAngle = 0f;
        ApplyRotate(0f);
    }

    public void ApplyRotate(float angle)
    {
        Vector3 wheelTransform = steeringWheel.transform.localEulerAngles;

        steeringWheel.transform.localRotation = Quaternion.Euler(wheelTransform.x, wheelTransform.y, angle);
    }

    public void UpdateWheel(WheelCollider col, Transform trans)
    {
        // Get wheel collider state
        Vector3 position;
        Quaternion rotation;
        col.GetWorldPose(out position, out rotation);
        
        // Set wheel transform state
        trans.position = position;
        trans.rotation = rotation;
    }
}
