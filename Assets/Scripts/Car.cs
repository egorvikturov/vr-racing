using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour, Interfaces.IMovable, Interfaces.IBreakable, Interfaces.IRotatable
{
    [Header("Wheels Colliders")]
    [SerializeField] private WheelCollider frontRight;
    [SerializeField] private WheelCollider frontLeft;
    [SerializeField] private WheelCollider backRight;
    [SerializeField] private WheelCollider backLeft;
    
    [Header("Wheels Transforms")]
    [SerializeField] private Transform frontRightTransform;
    [SerializeField] private Transform frontLeftTransform;
    [SerializeField] private Transform backRightTransform;
    [SerializeField] private Transform backLeftTransform;

    [Header("Steering Wheel")] 
    [SerializeField] private Transform steeringWheel;

    [Header("Car Settings")]
    [SerializeField] private float acceleration = 500f;
    [SerializeField] private float brakingForce = 300f;
    [SerializeField] private float maxTurnAngle = 45f;
    [SerializeField] private FMODEvents motorSound;

    private float soundAcceleration = 0f;
    private float currentAcceleration = 0f;
    private float currentBrakeForce = 0f;
    private float currentTurnAngle = 0f;
    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        UpdateWheel(frontRight, frontRightTransform);
        UpdateWheel(frontLeft, frontLeftTransform);
        UpdateWheel(backRight, backRightTransform);
        UpdateWheel(backLeft, backLeftTransform);
    }

    public void Move(float ratio)
    {
        //Debug.Log("Move!");
        currentAcceleration = acceleration * ratio;
        ApplyAcceleration();
    }

    public void ApplySound()
    {
        soundAcceleration = Mathf.InverseLerp(0, 15, rb.velocity.magnitude);
        motorSound.SetAccelerationParameter(soundAcceleration);
    }

    public void Brake()
    {
        //Debug.Log("Stop!");
        currentBrakeForce = brakingForce;
        ApplyBrakeForce();
    }

    public void StopBraking()
    {
        currentBrakeForce = 0f;
        ApplyBrakeForce();
    }

    public void StopMoving()
    {
        currentAcceleration = 0f;
        ApplyAcceleration();
    }

    public void ApplyAcceleration()
    {
        frontRight.motorTorque = currentAcceleration;
        frontLeft.motorTorque = currentAcceleration;
        ApplySound();
    }

    public void ApplyBrakeForce()
    {
        //backRight.brakeTorque = currentBrakeForce;
        //backLeft.brakeTorque = currentBrakeForce;
        frontRight.brakeTorque = currentBrakeForce;
        frontLeft.brakeTorque = currentBrakeForce;
        
    }

    public void Rotate(float ratio)
    {
        currentTurnAngle = maxTurnAngle * ratio;
        ApplyRotate(currentTurnAngle);
        //Debug.Log("I Rotate = " + currentTurnAngle);
    }

    public void StopRotating()
    {
        currentTurnAngle = 0f;
        ApplyRotate(0f);
    }

    public void ApplyRotate(float angle)
    {
        Vector3 wheelTransform = steeringWheel.transform.localEulerAngles;

        steeringWheel.transform.localRotation = Quaternion.Euler(wheelTransform.x, wheelTransform.y, angle);
        frontLeft.steerAngle = angle;
        frontRight.steerAngle = angle;
    }

    public void UpdateWheel(WheelCollider col, Transform trans)
    {
        // Get wheel collider state
        Vector3 position;
        Quaternion rotation;
        col.GetWorldPose(out position, out rotation);
        
        // Set wheel transform state
        trans.position = position;
        trans.rotation = rotation;
    }
}
